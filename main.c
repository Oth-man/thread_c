#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>

#define NB_VALUES_LINE_MAX 10//max of integer numbers in each line
#define NB_CHAR_LINE_MAX 200//max of characters in each line



struct line_struct {
    int array_values[NB_VALUES_LINE_MAX]; //each line representing a single-dimensional array and consists of 10 integer numbers
    int array_sum;//sum of the array
};
FILE *file;//declare a pointer of type file. This declaration is needed for communication between the file and program.
const char fileName[] = "file.txt";//file stored in a file system
const int NB_LINE_MAX = 100;//max number of threads
int arrayLength;//number of values in each line
void fileNextLine_to_array(int *array);//The first function must read from the given file the next line (array elements) and store them in an array defined as a parameter.
void* thread_CalculateSumArray(void* arg);//The second function calculates the sum of all array elements. This function is the one to be called by the created thread

/*The function main() accepts the following two command line parameters:
the number of threads to be created (one thread for each file line_array)
and the array length (number of values in each line).*/
int main(int argc, char const *argv[])
{
    //1

    //Validate the count of the parameters and display a proper message in case of wrong count.
    if ((argc-1) != 2)
    {
        printf("unexpected number of arguments\n");
        return -1;
    }

    int threadsCount =atoi(argv[1]);//the number of threads to be created
    int i;

    //Validate the number of threads (specified by the first command-line parameter)
    if (threadsCount <= 0)//if <= 0
    {
        // exit if the number of threads < 0 or = 0
        printf("the number of threads <= 0 !!\n");
        exit(-1);
    }
    if(threadsCount > NB_LINE_MAX)
    {
        // exit if the number of threads > 100
        printf("the number of threads > 100 !!\n");
        exit(-1);
    }
    arrayLength = atoi(argv[2]) ;//the array length (number of values in each line)

    //2

    //Validate the array length (specified by the second command-line parameter)
    if (arrayLength < 0)
    {
        // exit if the number of array elements < 0
        printf("the number of array length < 0 !!\n");
        exit(-1);
    }
    if (arrayLength == 0)
    {
        // exit if the number of array elements = 0
        printf("the number of array length = 0 !!\n");
        exit(-1);
    }
    if (arrayLength > NB_VALUES_LINE_MAX)
    {
        // exit if the number of array elements > 10
        printf("the number of array length > 10 !!\n");
        exit(-1);
    }

    //Given a text file containing up to 100 lines
    printf("\n");
    printf("------------------------------------------------------------------------------------\n");
    printf("|                              Read and display file                               |\n");
    printf("------------------------------------------------------------------------------------\n");

    //Opens the file whose name is specified in the parameter fileName for input operations ( r : Open for reading)
    file = fopen(fileName, "r");

    //The file must exist to continue
    if (file == NULL)
    {
        perror(fileName);//why didn't the file open?
        return -1;
    }

    struct line_struct args[threadsCount];//array of structures ( line_struct )

    //3

    //For all arrays whose count is specified by the first parameter
    for ( i = 0; i < threadsCount; i++)
    {
        //call the first function to copy the values from each line into an array and then display array elements at the beginning of a new line
        fileNextLine_to_array(args[i].array_values);
    }

    //Close the file
    fclose(file);
    printf("------------------------------------------------------------------------------------");


    //4
    printf("\n");
    printf("                  ------------------------------------------------\n");
    printf("                  |                Launch threads                |\n");
    printf("                  ------------------------------------------------\n");

    //Create the need number of threads (specified by the first command-line parameter) to call the second function for each array.
    pthread_t tids[threadsCount];//declare a array of type pthread_t to create the need number of threads
    for (i = 0; i < threadsCount; i++) {
        //Create the thread to call the second function
        pthread_create(&tids[i], NULL, thread_CalculateSumArray, &args[i]);
    }

    //5

    //Waits for threads to terminate
    for (i = 0; i < threadsCount; i++  )
    {
        pthread_join(tids[i], NULL);
    }

    for (i = 0; i < threadsCount; i++  )
    {
        //display the sum of each array
        printf("                  | Sum for thread [ %2d ] is  %18d |\n", i, args[i].array_sum);
    }
    printf("                  ------------------------------------------------");
    printf("\n");
    printf("                            ----------------------------\n");
    printf("                            |  My Programming Project  |\n");
    printf("                            ----------------------------\n");
    return 0;
}

void fileNextLine_to_array(int array[arrayLength])
{
    char str[NB_CHAR_LINE_MAX] = "";//a string of characters to store a line of the file
    // get the maximum number of characters from the file , store all in "str"
    fgets(str, NB_CHAR_LINE_MAX, file);
    // get the first token
    char *token = (char *)strtok(str, " ");
    /*Keep get tokens while one of the delimiters present in str[]
    and the number of integers in the line is inferior to that entered in the second command-line parameter */
    int i = 0;
    printf("| ");
    while (token != NULL && i < arrayLength)
    {
        //walk through other token
        //copy the values into the array
        array[i] = atoi(token);
        //display the array element
        printf("%8d", array[i]);
        token = (char *)strtok(NULL, " ");//A null pointer is returned if there are no tokens left to retrieve.
        //increment the number of integers in the line
        i++;
    }
    printf(" |\n");
}


void* thread_CalculateSumArray(void* arg)
{
    //Retrieving a line_struct
    struct line_struct *arg_struct = (struct line_struct*) arg;

    int sum = 0;
    int i;

    //for all values in the line
    for (i = 0; i < arrayLength; i++  )
    {
        //calculate the sum of the line
        sum += (*arg_struct).array_values[i];
    }
    //assign the sum calculated in the "array_sum" of the structure
    arg_struct->array_sum = sum;
    //terminate calling thread
    pthread_exit(0);
    return 0;
}

